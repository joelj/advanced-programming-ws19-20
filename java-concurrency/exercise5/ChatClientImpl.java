//@Joel_John
import java.rmi.server.*;
import java.rmi.*;

public class ChatClientImpl extends UnicastRemoteObject 
                            implements ChatClient {
  public String name;

  public ChatClientImpl() throws RemoteException {
   // name = null;
  }
  public void setName(String name) throws RemoteException {
    this.name = name;
  }
  public String getName() throws RemoteException {
    return this.name;
  }
  public boolean isAlive() throws RemoteException {
    return true;
  }
/** Receives a message from the server and prints it. */
  public void send(String msg) throws RemoteException{
    System.out.println(msg);

  }
}
