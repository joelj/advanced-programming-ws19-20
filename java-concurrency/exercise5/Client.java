//@Joel_John
import java.net.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.util.*;

public class Client {

  public static void main (String[] args) {
    try {
      String host = args.length >= 1 ? args[0] : "localhost";

      String uri = "rmi://" + host + "/Chatserver";

      Registry registry = LocateRegistry.getRegistry(65000);
      ChatServer o = (ChatServer)registry.lookup(uri);
      ChatClientImpl client = new ChatClientImpl();
      Scanner s = new Scanner(System.in);
      String choice = "/login";
      while(choice.equals("/login")){
        System.out.println("Connected to Chat. Please Enter a Username");
        String username = s.nextLine();
        while(!o.register(client, username))
        {
          System.out.println("Username already exists, please try a different one");
          username = s.nextLine();
        }
        System.out.println("Enter /exit to exit chat , Enter /online to get list of all online users, Enter /help for help");
        String msg = s.nextLine().trim();
        while(!msg.equals("/exit")){
          if(msg.equals("/online")){
            o.send(client,"/check");
            System.out.println(o.getUsers());
          }
          else if(msg.equals("/help")){
            System.out.println("Enter /exit to exit chat , Enter /online to get list of all online users, Enter /help for help");
          }
          else{
            o.send(client,msg);
          }
          msg = s.nextLine().trim();
        }
        o.logout(client);
        System.out.println("You have been logged out from the chat. To login again, Enter /login");
        choice = s.nextLine();
        while(!choice.equals("/login")){
          System.out.println("Invalid Input. To login again, Enter /login");
          choice = s.nextLine();
        }
      }

    }  catch (NotBoundException|RemoteException e) {
      System.out.println("Registry Exception " + e);
    }
  }
}
