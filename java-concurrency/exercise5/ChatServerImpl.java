//@Joel_John
import java.rmi.server.*;
import java.util.ArrayList;
import java.rmi.*;
import java.util.List;
import java.util.NoSuchElementException;

public class ChatServerImpl extends UnicastRemoteObject 
                            implements ChatServer {

  private List<ChatClient> clients = new ArrayList<ChatClient>();
  // private ChatClient currentclient = new ChatClientImpl();
  private List<String> temp = new ArrayList<String>();

  public ChatServerImpl() throws RemoteException {
    //state = false;
  }


  public synchronized boolean register(ChatClient c, String name) throws RemoteException{
    if(!this.getUsers().contains(name)){
      c.setName(name);
      clients.add(c);
      this.send(c, " Joined the Chat ");
      temp.add(c.getName());
      return true;
    }
    else
    {
      return false;
    }
  }

  /** Returns the collection of users currently logged in. */
  public synchronized List<String> getUsers() throws RemoteException{
    List<String> users = new ArrayList<String>();
    for (int i = 0; i < clients.size(); i++)
        users.add(clients.get(i).getName());
    return users;

  }

  /** Removes the given client from the server. */
  public synchronized void logout(ChatClient c) throws RemoteException{
     clients.remove(c);
     temp = this.getUsers();
     this.send(c," Left the Chat ");

  }

  /** Sends the given message to all connected clients. */
  public synchronized void send(ChatClient c, String msg) throws RemoteException{
   // System.out.println(msg);
   for (int i = 0; i < clients.size(); i++){
     try{
        clients.get(i).isAlive();
        c.isAlive();
        if(!msg.equals("/check"))
          clients.get(i).send(c.getName()+" : " +msg);
     }catch(Exception e){
        List<String> temp1 = temp;
        clients.remove(clients.get(i));
        temp1.removeAll(this.getUsers());
        temp = this.getUsers();
        for (int j = 0; j < clients.size(); j++)
          clients.get(j).send(temp1 +" Left the Chat ");
     }
   }
  }
}
