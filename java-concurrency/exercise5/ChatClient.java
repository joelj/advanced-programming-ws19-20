//@Joel_John
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatClient extends Remote
{
  public void setName(String name) throws RemoteException;
  /** Receives a message from the server and prints it. */
  public void send(String msg) throws RemoteException;
  public String getName() throws RemoteException;
  public boolean isAlive() throws RemoteException;
}