//@Joel_John
import java.net.*;
import java.rmi.*;
import java.rmi.registry.*;


public class Server {
  public static void main(String[] args) {
    try {
      String host = args.length >= 1 ? args[0] : "localhost";

      String uri = "rmi://" + host + "/Chatserver";

      ChatServerImpl serverObj = new ChatServerImpl();
      Registry registry = LocateRegistry.createRegistry(65000);
      registry.rebind(uri,serverObj);
    } catch (RemoteException e) {
      System.out.println("Registry Exception " + e);
    }
  }
}
